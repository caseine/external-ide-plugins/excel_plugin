﻿using System;
using System.Globalization;

namespace CaseInEExcelAddIn
{
    static class MultiLang
    {
        // Lang to be used by default
        public static string Lang = CultureInfo.CurrentUICulture.Name;

        public enum ID{
        // Common texts
        Ok,Save,Cancel,
        // Common errors
        Error,ServiceError,ConnectionError,UnknownError,
        // Common warning
        InvalidVplID,
        // Ribbon UI
        RibbonUI_Title,
        RibbonUI_ImportLabel,RibbonUI_ImportTooltip,
        RibbonUI_ExportLabel,RibbonUI_ExportTooltip,
        RibbonUI_EvaluateLabel,RibbonUI_EvaluateTooltip,RibbonUI_EvaluateOnlyTooltip,
        RibbonUI_ConfigLabel,RibbonUI_ConfigTooltip,
        RibbonUI_SubjectLabel,RibbonUI_SubjectTooltip,
        // Ribbon messages
        Ribbon_GetDataError,
        Ribbon_NoReqfilesWarning,Ribbon_NoReqfilesWarning_Title,
        Ribbon_TooManyReqfilesWarning,Ribbon_TooManyReqfilesWarning_Title,
        Ribbon_ConfirmImport,
        Ribbon_ImportError,
        Ribbon_OpenImportedWorkbookError,
        Ribbon_ImportSuccess,Ribbon_ImportSuccess_Title,
        Ribbon_PromptSaveAs,Ribbon_PromptSaveAs_Title,
        Ribbon_PromptSave,Ribbon_PromptSave_Title,
        Ribbon_BadFilenameError,
        Ribbon_ExportSuccess,Ribbon_ExportSuccess_Title,
        Ribbon_NoVplIDFound,Ribbon_NoVplIDFound_Title,
        Ribbon_CellInEditionError,
        Ribbon_PromptEvaluationRetry,

        // Configuration form UI
        ConfigUI_Title,
        ConfigUI_WelcomeText,
        ConfigUI_TokenPrompt,ConfigUI_RememberToken,
        ConfigUI_Advanced,
        ConfigUI_ExportOnEvaluation,
        ConfigUI_AutosaveOnExport,
        ConfigUI_PromptVplIDOnExport,
        // Configuration form messages
        Config_InvalidToken,
        
        // Import form UI
        ImportUI_Title,
        ImportUI_PromptText,
        ImportUI_InitialFilesLabel,ImportUI_InitialFilesTooltip,
        ImportUI_CurrentFilesLabel,ImportUI_CurrentFilesTooltip,
        ImportUI_FromVplIDPrompt,
        ImportUI_LocationPrompt,ImportUI_Browse,
        // Import form messages
        Import_InvalidFolder_,_Import_InvalidFolder,Import_InvalidFolder_Title,

        // Export form UI
        ExportUI_Title,
        ExportUI_VplIDPrompt,
        
        // Evaluation waiting form UI
        WaitUI_Title,
        WaitUI_Message,

		// Evaluation grade message
		Eval_GradeTitle,
		Eval_Grade,
		Eval_WrongAnswer,
		Eval_ExpectedAnswer
        }
        public static string[] en = {
        // Common texts
        "OK","Save","Cancel",
        // Common errors
        "Error","Service error","Connection error","An unknown error has occured.",
        // Common warning
        "Invalid VPL ID",
        // Ribbon UI
        "CaseInE Excel Add-in",
        "Import (pull)","Imports (pull) initial or current VPL files from CaseInE",
        "Export (push)","Exports (push) currently open workbook to CaseInE (new submission)",
        "Evaluate"," and evaluates the submission","Evaluates the last submission",
        "Configuration","Options : lang, token and behavior of the CaseInE add-in",
        "Exercise subject","Displays the exercise subject and description",
        // Ribbon messages
        "Error: unable to retrieve data from server.",
        "Warning: no requested file specified on CaseInE for this lab.\nSome services may not work properly.","No requested file specified",
        "Warning: too many requested files specified on CaseInE for this lab.\nSome services may not work properly.","Too many requested files",
        "Are you sure you want to import these files? Any existing files with the same name as one of the following will be replaced at location"/* <location>*/,
        "Error during import of file"/* <filename>*/,
        "Unable to open imported workbook.",
        "Files imported successfully.","Import successful",
        "Your workbook is not saved on the disk. You need to save it in order to export it. Save it now?","Save your workbook",
        "You have unsaved changes to your workbook. Do you want to save and include these changes in your submission?","Unsaved changes detected",
        "Error: bad file name:"/* <filename>*/,
        "Workbook exported successfully.","Export successful",
        "No VPL ID found. Please export workbook manually once (by using the \"Export\" button) to set up the VPL ID.","No VPL ID found",
        "Error: a cell is being edited - unable to write evaluation result. Please retry when no cell is being edited.",
        "Failed to retrieve evaluation result (server seems busy). Retry with greater delay?",

        // Configuration form UI
        "Configuration",
        "Welcome to the (friendly) CaseInE Excel Add-in configuration!",
        "Token:","Remember token",
        "Advanced",
        "Export (push) before evaluate",
        "Auto-save before export",
        "Always prompt VPL ID on export",
        // Configuration form messages
        "Invalid token",

        // Import form UI
        "Import files",
        "Which files do you want to import?",
        "Initial files","Import VPL requested files from CaseInE at their initial state",
        "Current files","Import (pull) VPL files from CaseInE at their current (last submission) state",
        "From VPL ID:",
        "To location:","Browse...",
        // Import form messages
        "Folder path"/* <foldername> */,"is invalid.","Invalid path",
        
        // Export form UI
        "Export workbook",
        "Export workbook to VPL ID:",
        
        // Evaluation waiting form UI
        "Please wait",
        "Evaluation is in progress, please wait...",

		// Evaluation grade message
		"Evaluation result",
		"Grade:"/* <grade>*/,
		"Wrong answer!",
		"Expected answer:"/* <expected answer>*/
        };

        public static string[] fr = {
        // Common texts
        "OK","Enregistrer","Annuler",
        // Common errors
        "Erreur ","Erreur du service ","Erreur de connexion ","Une erreur inconnue est survenue.",
        // Common warning
        "ID VPL invalide",
        // Ribbon UI
        "Complément Excel CaseInE",
        "Importer (pull)","Importe (pull) les fichiers initiaux ou actuels du VPL depuis CaseInE",
        "Exporter (push)","Exporte (push) le classeur actuellement ouvert vers CaseInE (crée une nouvelle soumission)",
        "Evaluer"," et évalue la soumission","Evalue la dernière soumission",
        "Configuration","Options : langue, token et comportement du complément CaseInE",
        "Sujet","Affiche le sujet et la description de l'exercice",
        // Ribbon messages
        "Erreur : \nImpossible de récupérer les données depuis le serveur.",
        "Attention : aucun fichier requis n'a été spécifié pour cet exercice sur CaseInE.\nCertains services risquent de ne pas fonctionner.","Pas de fichier requis spécifié",
        "Attention : trop de fichiers requis specifiés pour cet exercice sur CaseInE.\nCertains services risquent de ne pas fonctionner.","Trop de fichiers requis spécifiés",
        "Êtes-vous sûr de vouloir importer ces fichiers ? Tout fichier existant du même nom qu'un des fichiers suivants sera remplacé à l'emplacement"/* <location>*/,
        "Erreur lors de l'importation du fichier"/* <filename>*/,
        "Impossible d'ouvrir le classeur importé.",
        "Les fichiers ont été importés avec succès.","Succès de l'importation",
        "Votre classeur n'est pas enregistré sur le disque. L'exportation requiert qu'il soit enregistré. Souhaitez-vous l'enregistrer maintenant ?","Enregistrer le classeur",
        "Votre classeur a des modifications non enregistrées. Voulez-vous les enregistrer et les inclure dans votre soumission ?","Modifications non enregistrées",
        "Erreur : nom de fichier invalide :"/* <filename>*/,
        "Le classeur a été exporté avec succès.","Succès de l'exportation",
        "Pas d'ID VPL trouvé. Veuillez exporter une fois manuellement le classeur (en utilisant le bouton \"Exporter\").","Pas d'ID VPL trouvé",
        "Erreur : une cellule est en cours d'édition - impossible d'afficher le résultat de l'évaluation. Veuillez réessayer quand aucune cellule n'est en cours d'édition.",
        "Echec de récupération des données d'évaluation (le serveur est peut-être surchargé). Réessayer avec un délai plus important ?",

        // Configuration form UI
        "Configuration",
        "Bienvenue dans la configuration du complément Excel CaseInE !",
        "Token :","Mémoriser le token",
        "Avancé",
        "Exporter (push) avant d'évaluer",
        "Enregistrer automatiquement avant d'exporter",
        "Toujours demander l'ID VPL pour l'exportation",
        // Configuration form messages
        "Token invalide",
        
        // Import form UI
        "Importer des fichiers",
        "Quels fichiers voulez-vous importer ?",
        "Fichiers initiaux","Importe les fichiers du VPL depuis CaseInE dans leur état initial",
        "Fichiers actuels","Importe (pull) les fichiers du VPL depuis CaseInE dans leur état actuel (dernière soumission)",
        "Depuis l'ID VPL :",
        "A l'emplacement :","Parcourir...",
        // Import form messages
        "Le chemin"/* <foldername> */,"est invalide.","Chemin invalide",
        
        // Export form UI
        "Exporter le classeur",
        "Exporter le classeur vers l'ID VPL :",

        // Evaluation waiting form UI
        "Veuillez patienter",
        "Evaluation en cours, veuillez patienter...",

		// Evaluation grade message
		"Résultat de l'évaluation",
		"Note :"/* <grade>*/,
		"Mauvaise réponse !",
		"Réponse attendue :"/* <expected answer>*/
        };
        
        public static string OKText(){
            return Text(ID.Ok);
        }
        public static string CancelText(){
            return Text(ID.Cancel);
        }

        public static string ErrorText(){
            return Text(ID.Error);
        }
        public static string ServiceErrorText(){
            return Text(ID.ServiceError);
        }
        public static string ConnectionErrorText(){
            return Text(ID.ConnectionError);
        }
        public static string UnknownErrorText(){
            return Text(ID.ConnectionError);
        }

        public static string InvalidVplIDText(){
            return Text(ID.InvalidVplID);
        }

        public static string Text(ID id){
            return Text(Lang,id);
        }

        public static string Text(string lang, ID id){
            try{
                switch(lang){
                    case "fr-FR": return fr[(int)id];
                    case "en-GB":
                    case "en-US":
                    default: return en[(int)id];
                }
            }
            catch(Exception){
                return "<Text_"+lang+"_"+id+">";
            }
        }
    }
}
