﻿namespace CaseInEExcelAddIn
{
    internal class JsonCaseineObject
    {
        //#info#
        public string Name {get;set;}
        public string Shortdescription {get;set;}
        public string Intro {get;set;}
        public int Introformat {get;set;}
        public int Reqpassword {get;set;}
        public int Example {get;set;}
        public int Restrictededitor {get;set;}
        public CaseineFile[] Reqfiles {get;set;}
        //######
        //#open#
        public CaseineFile[] Files {get;set;}
        //#get_result#
        public string Compilation {get;set;}
        public string Evaluation {get;set;}
        public string Grade {get;set;}
        //############
        //######
        //#evaluate#
        public object MonitorURL {get;set;}
        //##########
        //#exception#
        public string Exception {get;set;}
        public string Errorcode {get;set;}
        public string Message {get;set;}
        //###########
    }

    internal class CaseineFile
    {
        public string name {get;set;}
        public string data {get;set;}
    }
}