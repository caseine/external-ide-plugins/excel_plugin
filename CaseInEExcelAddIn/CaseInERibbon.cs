﻿using System;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Tools.Ribbon;
using System.IO;
using System.Web.Script.Serialization;
using Microsoft.Office.Core;
using System.Net;
using System.Threading;

namespace CaseInEExcelAddIn
{
    public partial class CaseInERibbon
    {
        /*
        VPL conditions for this Add-in to work :
        - Exactly one requested file
        - The name of the requested file must fit the name of the evaluated file
        - The requested file must be in format .b64

        If several requested files are specified, all of them will be imported on initial import
        However, only the first will be treated for current import / export
        */
        
        // Name of the folder in %AppData%
        private const string UserDataSubPath = "CaseInE";
        // Name of the vpl id in workbooks' metadata
        private const string VplIDMetadataName = "IDVPL";

        // ID of the VPL
        internal int VplID {get; private set;}
        // Local path for import
        internal static string ImportLocation {get; private set;}

        // Token of the student
        internal string Token {get;set;}

        // Whether the evaluate button triggers an export or not
        private bool exportOnEvaluation;
        internal bool ExportOnEvaluation
        {
            get => exportOnEvaluation;
            set
            {
                exportOnEvaluation = value;
                if(value)
                    evaluateButton.SuperTip = exportButton.SuperTip + MultiLang.Text(MultiLang.ID.RibbonUI_EvaluateTooltip);
                else
                    evaluateButton.SuperTip = MultiLang.Text(MultiLang.ID.RibbonUI_EvaluateOnlyTooltip);
            }
        }
        // Whether the export automatically triggers a save on active workbook
        internal bool AutosaveOnExport {get;set;}
        // Whether the addin should prompt the user for VplID on each export or use the saved one
        internal bool PromptVplIDOnExport {get;set;}

        // Location of the preferences files : %AppData%\CaseInE
        internal string UserDataDirectory {get; private set;}
        // Name of the file where the token is stored
        internal string UserTokenFile {get; private set;}
        // Name of the file where the user preferences are stored
        internal string UserPreferencesFile {get; private set;}

		// If files are currently imported or not
		// This is used to prevent conflicts on VplID read/write on metadata between OnWorkbookActivate and ImportFiles
		private bool InImportProcess = false;

        /// <summary>
        /// Ribbon initialization.
        /// </summary>
        private void CaseInERibbon_Load(object sender, RibbonUIEventArgs e)
        {
            try{
                // Set up the preferences files (create folder and files)
                UserDataDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),UserDataSubPath);
                Directory.CreateDirectory(UserDataDirectory);
                UserTokenFile = Path.Combine(UserDataDirectory,"token");
                if(!File.Exists(UserTokenFile))
                    File.Create(UserTokenFile);
                UserPreferencesFile = Path.Combine(UserDataDirectory,"user_preferences");
                if(!File.Exists(UserPreferencesFile))
                    File.Create(UserPreferencesFile);
            }
            catch(Exception){
                // A problem occured, display error message and disable the ribbon
                // (if a problem appears here on simple IO on filesystem, the whole ribbon won't work)
                MessageBox.Show("Error setting up CaseInE add-in.\nPlease contact the support.","Error",
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                foreach(Control c in this.group1.Items){
                    c.Enabled = false;
                }
                group1.Visible = false;
                return;
            }

            // On Ribbon load, no file is open yet, so we can't retrieve id metadata from currently open file
            VplID = -1;

            // Initialize token by reading saved token file
            try{
                using(FileStream f = File.Open(UserTokenFile,FileMode.Open,FileAccess.Read,FileShare.Read)){
                using(StreamReader s = new StreamReader(f)){
                    Token = s.ReadToEnd();
                }
                }
            }
            catch(Exception){
                Token = "";
            }

            // Initialize lang and advanced preferences by reading saved preferences file
            try{
                JsonPreferencesObject preferences = null;
                // Deserialize user preferences JSon file
                using(FileStream f = File.Open(UserPreferencesFile,FileMode.Open,FileAccess.Read,FileShare.Read)){
                using(StreamReader s = new StreamReader(f)){
                    string JSon_pref = s.ReadToEnd();
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    preferences = (JsonPreferencesObject)js.Deserialize(JSon_pref,typeof(JsonPreferencesObject));
                }
                }
                MultiLang.Lang = preferences.Lang;
                ExportOnEvaluation = preferences.ExportOnEvaluation;
                AutosaveOnExport = preferences.AutosaveOnExport;
                PromptVplIDOnExport = preferences.PromptVplIDOnExport;
                ImportLocation = preferences.ImportLocation;
            }
            catch(Exception){
                MultiLang.Lang = System.Globalization.CultureInfo.CurrentUICulture.Name;
                ExportOnEvaluation = true;
                AutosaveOnExport = false;
                PromptVplIDOnExport = true;
                ImportLocation = Globals.ThisAddIn.Application.DefaultFilePath;
            }
            
            // Load text from MultiLang
            ApplyLang();

            // Set up the WorkBookActivatedHandler
            Globals.ThisAddIn.Application.WorkbookActivate += new AppEvents_WorkbookActivateEventHandler(OnWorkBookActivate);
        }

        /// <summary>
        /// Imports (pull) initial or current files from CaseInE.
        /// </summary>
        private void ImportButton_Click(object sender,RibbonControlEventArgs e)
        {
            if(Token==""){
                DialogResult configResult = OpenConfigForm();
                if(configResult == DialogResult.Cancel)
                    return;
            }

            int oldVplID = VplID;

            try{
                using(FormImport form = new FormImport(VplID,ImportLocation)){
                    form.ShowDialog();
                    DialogResult result = form.GetDialogResult();
                    if(result == DialogResult.OK){
                        VplID = form.GetChosenVplID();
                        ImportLocation = form.GetChosenPath();

                        // Write import location to preferences
                        JsonPreferencesObject preferences = null;
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        try{
                            // Deserialize user preferences JSon file
                            using(FileStream f = File.Open(UserPreferencesFile,FileMode.Open,FileAccess.Read,FileShare.Read)){
                            using(StreamReader s = new StreamReader(f)){
                                string JSonPref = s.ReadToEnd();
                                preferences = (JsonPreferencesObject)js.Deserialize(JSonPref,typeof(JsonPreferencesObject));
                                preferences.ImportLocation = ImportLocation;
                            }
                            }
                        }
                        catch(Exception){
                            preferences = new JsonPreferencesObject(true,System.Globalization.CultureInfo.CurrentUICulture.Name,true,false,true,ImportLocation);
                        }
                        try{
                            // Write new preferences
                            Directory.CreateDirectory(UserDataDirectory);
                            File.WriteAllText(UserPreferencesFile,js.Serialize(preferences));
                        }
                        catch(Exception){
                            // Not supposed to happen if user rights on filesystem didn't change since startup
                        }
                        
                        FormImport.FilesChoice choice = form.GetFilesChoice();
                        bool importOk;
						InImportProcess = true;

						if (choice == FormImport.FilesChoice.Current){
                            importOk = ImportCurrentFiles();
                        }
                        else{
                            importOk = ImportInitialFiles();
                        }
                        if(importOk){
                            ApplyVplIDToWorkbook(VplID);
                        }
                        else
                            VplID = oldVplID;

						InImportProcess = false;
					}
                }
            }
            catch(IOException){
                // Failed to open imported workbook
                VplID = oldVplID;
				InImportProcess = false;
				MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_OpenImportedWorkbookError),MultiLang.Text(MultiLang.ID.Ribbon_OpenImportedWorkbookError),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
            catch{
                // Unknown error
                VplID = oldVplID;
				InImportProcess = false;
				MessageBox.Show(MultiLang.UnknownErrorText(),MultiLang.ErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// Exports (push) local files to CaseInE.
        /// </summary>
        private void ExportButton_Click(object sender,RibbonControlEventArgs e)
        {
            if(Token==""){
                DialogResult configResult = OpenConfigForm();
                if(configResult == DialogResult.Cancel)
                    return;
            }
            
            try{
                if(PromptVplIDOnExport || VplID == -1){
                    using(FormExport form = new FormExport(VplID)){
                        form.ShowDialog();
                        DialogResult result = form.GetDialogResult();
                        if(result == DialogResult.OK){
                            VplID = form.GetChosenVplID();
                            ApplyVplIDToWorkbook(VplID);
                        }
                        else

                            return;
                    }
                }
                ExportFile();
            }
            catch{
                MessageBox.Show(MultiLang.UnknownErrorText(),MultiLang.ErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// Evaluates submission and displays evaluation result in workbook.
        /// </summary>
        private void EvaluateButton_Click(object sender,RibbonControlEventArgs e)
        {
            if(VplID==-1){
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_NoVplIDFound),MultiLang.Text(MultiLang.ID.Ribbon_NoVplIDFound_Title),
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            if(Token==""){
                DialogResult configResult = OpenConfigForm();
                if(configResult == DialogResult.Cancel)
                    return;
            }
            
            try{
                if(ExportOnEvaluation){
                    // Export before evaluate
                    bool exportOk = ExportFile(displayInfo:false);
                    if(!exportOk)
                        return;
                }
                
                Evaluate();
            }
            catch(Exception ex){
                MessageBox.Show(MultiLang.Text(MultiLang.ID.UnknownError) +" : "+ ex.Message,MultiLang.ErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
				MessageBox.Show(ex.StackTrace);
                return;
            }
        }

        /// <summary>
        /// Opens the config form.
        /// </summary>
        private void ConfigButton_Click(object sender,RibbonControlEventArgs e)
        {
            OpenConfigForm();
        }
        
        /// <summary>
        /// Displays VPL subject description in a separate window.
        /// </summary>
        private void SubjectButton_Click(object sender,RibbonControlEventArgs e)
        {
            if(VplID==-1){
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_NoVplIDFound),MultiLang.Text(MultiLang.ID.Ribbon_NoVplIDFound_Title),
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            try{
                DisplaySubject();
            }
            catch{
                MessageBox.Show(MultiLang.Text(MultiLang.ID.UnknownError),MultiLang.ErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
        }

        /// <summary>
        /// On activation of a workbook, retrieves the Vpl ID from file's metadata.
        /// </summary>
        private void OnWorkBookActivate(Workbook workbook)
        {
            try{
				if(!InImportProcess){
					if(workbook.Path.Length > 0){
						// Path is non empty, the workbook is saved on the disk
						bool found = false;
						foreach(DocumentProperty p in workbook.CustomDocumentProperties){
							if(p.Name==VplIDMetadataName){
								found = true;
								VplID = p.Value;
							}
						}
						if(!found){
							VplID = -1;
						}
					}
					else{
						VplID = -1;
					}
				}
            }
            catch{
                VplID = -1;
                MessageBox.Show(MultiLang.Text(MultiLang.ID.UnknownError),MultiLang.ErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
        }
    }
}