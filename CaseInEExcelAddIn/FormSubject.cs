﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaseInEExcelAddIn
{
    public partial class FormSubject:Form
    {
        public FormSubject(string title="", string htmlContents="")
        {
            InitializeComponent();
            this.Text = title;
            webDisplayer.DocumentText = htmlContents;
        }
    }
}
