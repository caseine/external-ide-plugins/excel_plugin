﻿namespace CaseInEExcelAddIn
{
    partial class CaseInERibbon:Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public CaseInERibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CaseInERibbon));
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.importButton = this.Factory.CreateRibbonButton();
            this.exportButton = this.Factory.CreateRibbonButton();
            this.evaluateButton = this.Factory.CreateRibbonButton();
            this.configButton = this.Factory.CreateRibbonButton();
            this.subjectButton = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.importButton);
            this.group1.Items.Add(this.exportButton);
            this.group1.Items.Add(this.evaluateButton);
            this.group1.Items.Add(this.configButton);
            this.group1.Items.Add(this.subjectButton);
            this.group1.Label = "CaseInE";
            this.group1.Name = "group1";
            // 
            // importButton
            // 
            this.importButton.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine_import;
            this.importButton.Label = "import";
            this.importButton.Name = "importButton";
            this.importButton.ShowImage = true;
            this.importButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ImportButton_Click);
            // 
            // exportButton
            // 
            this.exportButton.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine_export;
            this.exportButton.Label = "export";
            this.exportButton.Name = "exportButton";
            this.exportButton.ShowImage = true;
            this.exportButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ExportButton_Click);
            // 
            // evaluateButton
            // 
            this.evaluateButton.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine_check;
            this.evaluateButton.Label = "evaluate";
            this.evaluateButton.Name = "evaluateButton";
            this.evaluateButton.ShowImage = true;
            this.evaluateButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.EvaluateButton_Click);
            // 
            // configButton
            // 
            this.configButton.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine_config;
            this.configButton.Label = "config";
            this.configButton.Name = "configButton";
            this.configButton.ShowImage = true;
            this.configButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ConfigButton_Click);
            // 
            // subjectButton
            // 
            this.subjectButton.Image = ((System.Drawing.Image)(resources.GetObject("subjectButton.Image")));
            this.subjectButton.Label = "subject";
            this.subjectButton.Name = "subjectButton";
            this.subjectButton.ShowImage = true;
            this.subjectButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SubjectButton_Click);
            // 
            // CaseInERibbon
            // 
            this.Name = "CaseInERibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.CaseInERibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton exportButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton evaluateButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton configButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton importButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton subjectButton;
    }

    partial class ThisRibbonCollection
    {
        internal CaseInERibbon CaseInERibbon
        {
            get { return this.GetRibbon<CaseInERibbon>(); }
        }
    }
}
