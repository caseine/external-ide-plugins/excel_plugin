﻿namespace CaseInEExcelAddIn
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormImport));
			this.label1 = new System.Windows.Forms.Label();
			this.cancelButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.currentFilesButton = new System.Windows.Forms.RadioButton();
			this.initialFilesButton = new System.Windows.Forms.RadioButton();
			this.okButton = new System.Windows.Forms.Button();
			this.vplIDTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.localPathTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.browseButton = new System.Windows.Forms.Button();
			this.folderDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 6);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(59, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "prompt";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cancelButton
			// 
			this.cancelButton.Location = new System.Drawing.Point(306, 164);
			this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(104, 27);
			this.cancelButton.TabIndex = 5;
			this.cancelButton.Text = "cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.currentFilesButton);
			this.panel1.Controls.Add(this.initialFilesButton);
			this.panel1.Location = new System.Drawing.Point(18, 40);
			this.panel1.Margin = new System.Windows.Forms.Padding(2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(117, 60);
			this.panel1.TabIndex = 1;
			// 
			// currentFilesButton
			// 
			this.currentFilesButton.AutoSize = true;
			this.currentFilesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.currentFilesButton.Location = new System.Drawing.Point(2, 31);
			this.currentFilesButton.Margin = new System.Windows.Forms.Padding(2);
			this.currentFilesButton.Name = "currentFilesButton";
			this.currentFilesButton.Size = new System.Drawing.Size(71, 21);
			this.currentFilesButton.TabIndex = 1;
			this.currentFilesButton.TabStop = true;
			this.currentFilesButton.Text = "current";
			this.currentFilesButton.UseVisualStyleBackColor = true;
			// 
			// initialFilesButton
			// 
			this.initialFilesButton.AutoSize = true;
			this.initialFilesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.initialFilesButton.Location = new System.Drawing.Point(2, 8);
			this.initialFilesButton.Margin = new System.Windows.Forms.Padding(2);
			this.initialFilesButton.Name = "initialFilesButton";
			this.initialFilesButton.Size = new System.Drawing.Size(58, 21);
			this.initialFilesButton.TabIndex = 0;
			this.initialFilesButton.TabStop = true;
			this.initialFilesButton.Text = "initial";
			this.initialFilesButton.UseVisualStyleBackColor = true;
			// 
			// okButton
			// 
			this.okButton.Location = new System.Drawing.Point(198, 164);
			this.okButton.Margin = new System.Windows.Forms.Padding(2);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(104, 27);
			this.okButton.TabIndex = 4;
			this.okButton.Text = "ok";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.OKButton_Click);
			// 
			// vplIDTextBox
			// 
			this.vplIDTextBox.Location = new System.Drawing.Point(154, 57);
			this.vplIDTextBox.Margin = new System.Windows.Forms.Padding(2);
			this.vplIDTextBox.Name = "vplIDTextBox";
			this.vplIDTextBox.Size = new System.Drawing.Size(256, 20);
			this.vplIDTextBox.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(151, 38);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(38, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "from id";
			// 
			// localPathTextBox
			// 
			this.localPathTextBox.Location = new System.Drawing.Point(154, 104);
			this.localPathTextBox.Margin = new System.Windows.Forms.Padding(2);
			this.localPathTextBox.Name = "localPathTextBox";
			this.localPathTextBox.Size = new System.Drawing.Size(256, 20);
			this.localPathTextBox.TabIndex = 9;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(151, 85);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "to location";
			// 
			// browseButton
			// 
			this.browseButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.browseButton.AutoSize = true;
			this.browseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.browseButton.Location = new System.Drawing.Point(359, 128);
			this.browseButton.Margin = new System.Windows.Forms.Padding(2);
			this.browseButton.Name = "browseButton";
			this.browseButton.Size = new System.Drawing.Size(51, 23);
			this.browseButton.TabIndex = 11;
			this.browseButton.Text = "browse";
			this.browseButton.UseVisualStyleBackColor = true;
			this.browseButton.Click += new System.EventHandler(this.BrowseButton_Click);
			// 
			// folderDialog
			// 
			this.folderDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
			// 
			// FormImport
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(421, 202);
			this.Controls.Add(this.browseButton);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.localPathTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.vplIDTextBox);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormImport";
			this.Text = "import";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton currentFilesButton;
        private System.Windows.Forms.RadioButton initialFilesButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TextBox vplIDTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox localPathTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.FolderBrowserDialog folderDialog;
    }
}