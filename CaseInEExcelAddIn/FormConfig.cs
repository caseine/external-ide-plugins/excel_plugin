﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Web.Script.Serialization;

namespace CaseInEExcelAddIn
{
    public partial class FormConfig:Form
    {
        // Static state of the remember token checkbox
        private static bool rememberToken = true;

        // Associated ribbon (to apply changes)
        private CaseInERibbon ribbon;
        // Result of the dialog - done manually to prevent unverified validation
        private DialogResult formDialogResult;

        /// <summary>
        /// Form initialization.
        /// </summary>
        public FormConfig(CaseInERibbon ribbon){
            InitializeComponent();
            this.ribbon = ribbon;

            ApplyLang();
        }

        /// <summary>
        /// Loads text from MultiLang.
        /// </summary>
        public void ApplyLang(){
            this.Text = MultiLang.Text(MultiLang.ID.ConfigUI_Title);
            welcomeLabel.Text = MultiLang.Text(MultiLang.ID.ConfigUI_WelcomeText);
            tokenLabel.Text = MultiLang.Text(MultiLang.ID.ConfigUI_TokenPrompt);
            rememberMeCheck.Text = MultiLang.Text(MultiLang.ID.ConfigUI_RememberToken);
            advancedLabel.Text = MultiLang.Text(MultiLang.ID.ConfigUI_Advanced);
            exportOnEvaluationCheck.Text = MultiLang.Text(MultiLang.ID.ConfigUI_ExportOnEvaluation);
            autoSaveOnExportCheck.Text = MultiLang.Text(MultiLang.ID.ConfigUI_AutosaveOnExport);
            promptVplIDOnExportCheck.Text = MultiLang.Text(MultiLang.ID.ConfigUI_PromptVplIDOnExport);
            saveButton.Text = MultiLang.Text(MultiLang.ID.Save);
            cancelButton.Text = MultiLang.CancelText();
        }

        /// <summary>
        /// Loads the form according to the ribbon.
        /// </summary>
        private void FormConfig_Load(object sender,EventArgs e)
        {
            if(ribbon==null){
                // Really not supposed to happen
                MessageBox.Show("Null reference error - unable to open Config form.\nPlease contact the support.",
                    "Critical Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                try{
                    this.Close();
                }
                catch{
                }
                return;
            }
            
            // Load the token
            string token = ribbon.Token;
            tokenTextbox.Text = token;
            
            // Load additional info
            exportOnEvaluationCheck.Checked = ribbon.ExportOnEvaluation;
            autoSaveOnExportCheck.Checked = ribbon.AutosaveOnExport;
            promptVplIDOnExportCheck.Checked = ribbon.PromptVplIDOnExport;
            rememberMeCheck.Checked = rememberToken;
            
            // At start, advanced options are collapsed
            advancedOptionsContainer.Panel2Collapsed = true;

            // Display the lang flag according to current settings
            switch(MultiLang.Lang){
                case "fr-FR": FlagBox.Image = Properties.Resources.caseine_fr_flag;
                        break;
                case "en-GB":
                case "en-US":
                default: FlagBox.Image = Properties.Resources.caseine_en_flag;
                        break;
            }

            // Manual dialog result
            formDialogResult = DialogResult.None;
        }
        
        /// <summary>
        /// Tries to save settings to ribbon and preference files, and closes the form with OK result.
        /// If some setting is badly set, displays a warning and does not close the form.
        /// </summary>
        private void SaveButton_Click(object sender,EventArgs e)
        {
            // Verify token syntax (hexadecimal and length 32),
            // then write it to file and send it to ribbon
            string token = tokenTextbox.Text;
            try{
                if(token.Length != 32)
                    throw new Exception();
                for(int i=0;i<32;i+=4){
                    Convert.ToInt32(token.Substring(i,4),16);
                }
            }
            catch(Exception){
                string invalidTokenText = MultiLang.Text(MultiLang.ID.Config_InvalidToken);
                MessageBox.Show(invalidTokenText,invalidTokenText,
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            ribbon.Token = token;
            if(rememberMeCheck.Checked){
                rememberToken = true;
                try{
                    Directory.CreateDirectory(ribbon.UserDataDirectory);
                    File.WriteAllText(ribbon.UserTokenFile,token);
                }
                catch(Exception){
                    // Not supposed to happen if user rights on filesystem didn't change since startup
                    MessageBox.Show("There was a problem saving your token. " +
                        "It has been taken into account, but you will have to set it up again next time",
                        "Error : token not saved",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }
            }
            else{
                rememberToken = false;
                try{
                    if(File.Exists(ribbon.UserTokenFile))
                        File.Delete(ribbon.UserTokenFile);
                }
                catch{
                    // Not really a problem if something doesn't work here
                }
            }

            // Write preferences as a JSon string into user preferences file and send it to ribbon
            try{
                Directory.CreateDirectory(ribbon.UserDataDirectory);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string preferences = js.Serialize(new JsonPreferencesObject(rememberMeCheck.Checked,MultiLang.Lang,exportOnEvaluationCheck.Checked,autoSaveOnExportCheck.Checked,promptVplIDOnExportCheck.Checked,CaseInERibbon.ImportLocation));
                File.WriteAllText(ribbon.UserPreferencesFile,preferences);
            }
            catch(Exception){
                // Not supposed to happen if user rights on filesystem didn't change since startup
                MessageBox.Show("There was a problem saving your preferences. " +
                    "They have been taken into account, but you will have to set them up again next time",
                    "Error : preferences not saved",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            ribbon.ExportOnEvaluation = exportOnEvaluationCheck.Checked;
            ribbon.AutosaveOnExport = autoSaveOnExportCheck.Checked;
            ribbon.PromptVplIDOnExport = promptVplIDOnExportCheck.Checked;
            
            // If everything worked fine, close the form and set result to OK
            formDialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// Closes the form without saving and sets result to Cancel
        /// </summary>
        private void CancelButton_Click(object sender,EventArgs e)
        {
            formDialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Expands or collapses the Advanced panel.
        /// Sets the arrow image consistently
        /// </summary>
        private void AdvancedButton_Click(object sender,EventArgs e)
        {
            if(advancedOptionsContainer.Panel2Collapsed){
                advancedOptionsContainer.Panel2Collapsed = false;
                advancedButton.BackgroundImage = Properties.Resources.caseine_expanded_arrow;
            }
            else{
                advancedOptionsContainer.Panel2Collapsed = true;
                advancedButton.BackgroundImage = Properties.Resources.caseine_collapsed_arrow;
            }
        }

        /// <summary>
        /// Displays the lang choice flags panel
        /// </summary>
        private void FlagBox_Click(object sender,EventArgs e)
        {
            flagPanel.Visible = true;
            flagPanel.Enabled = true;
        }
        
        /// <summary>
        /// Applies new lang choice and writes it to user preferences file
        /// </summary>
        private void ChangeLang(string newLang){
            MultiLang.Lang = newLang;
            
            JsonPreferencesObject preferences = null;
            JavaScriptSerializer js = new JavaScriptSerializer();
            try{
                // Deserialize user preferences JSon file
                using(FileStream f = File.Open(ribbon.UserPreferencesFile,FileMode.Open,FileAccess.Read,FileShare.Read)){
                using(StreamReader s = new StreamReader(f)){
                    string JSonPref = s.ReadToEnd();
                    preferences = (JsonPreferencesObject)js.Deserialize(JSonPref,typeof(JsonPreferencesObject));
                    preferences.Lang = newLang;
                }
                }
            }
            catch(Exception){
                preferences = new JsonPreferencesObject(true,newLang,true,false,true,Globals.ThisAddIn.Application.DefaultFilePath);
            }
            try{
                // Write new preferences
                File.WriteAllText(ribbon.UserPreferencesFile,js.Serialize(preferences));
            }
            catch(Exception){
                // Not supposed to happen if user rights on filesystem didn't change since startup
                MessageBox.Show("There was a problem saving your lang preference. " +
                    "It has been taken into account, but you will have to set it up again next time",
                    "Error : lang not saved",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }

            this.ApplyLang();
            ribbon.ApplyLang();
            flagPanel.Visible = false;
            flagPanel.Enabled = false;
        }

        /// <summary>
        /// Changes image and lang to FR
        /// </summary>
        private void FrFlagBox_Click(object sender,EventArgs e)
        {
            FlagBox.Image = Properties.Resources.caseine_fr_flag;
            ChangeLang("fr-FR");
        }
        
        /// <summary>
        /// Changes image and lang to EN
        /// </summary>
        private void EnFlagBox_Click(object sender,EventArgs e)
        {
            FlagBox.Image = Properties.Resources.caseine_en_flag;
            ChangeLang("en-GB");
        }

        /// <summary>
        /// Returns the DialogResult of the form
        /// Should only be called after form has been displayed and closed
        /// </summary>
        public DialogResult GetDialogResult(){
            return formDialogResult;
        }
    }
}
