﻿namespace CaseInEExcelAddIn
{
    partial class FormSubject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webDisplayer = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webDisplayer
            // 
            this.webDisplayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webDisplayer.Location = new System.Drawing.Point(0, 0);
            this.webDisplayer.MinimumSize = new System.Drawing.Size(20, 20);
            this.webDisplayer.Name = "webDisplayer";
            this.webDisplayer.Size = new System.Drawing.Size(800, 450);
            this.webDisplayer.TabIndex = 0;
            // 
            // FormSubject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.webDisplayer);
            this.Name = "FormSubject";
            this.Text = "FormSubject";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webDisplayer;
    }
}