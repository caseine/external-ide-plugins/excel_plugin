﻿using System;
using System.IO;
using System.Windows.Forms;

namespace CaseInEExcelAddIn
{
    public partial class FormImport:Form
    {
        // Choice for import
        public enum FilesChoice{Initial,Current}
        private FilesChoice filesChoice;
        // Result of the dialog - done manually to prevent unverified validation
        private DialogResult formDialogResult;
        // Choice for vpl id
        private int chosenVplID;
        // Choice for local path
        private string chosenPath;

        /// <summary>
        /// Form initialization.
        /// </summary>
        public FormImport(int vplID=-1, string path="")
        {
            InitializeComponent();

            formDialogResult = DialogResult.None;

            // Load text from MultiLang
            this.Text = MultiLang.Text(MultiLang.ID.ImportUI_Title);
            label1.Text = MultiLang.Text(MultiLang.ID.ImportUI_PromptText);
            initialFilesButton.Text = MultiLang.Text(MultiLang.ID.ImportUI_InitialFilesLabel);
            currentFilesButton.Text = MultiLang.Text(MultiLang.ID.ImportUI_CurrentFilesLabel);
            label2.Text = MultiLang.Text(MultiLang.ID.ImportUI_FromVplIDPrompt);
            vplIDTextBox.Text = (vplID==-1 ? "" : vplID.ToString());
            label3.Text = MultiLang.Text(MultiLang.ID.ImportUI_LocationPrompt);
            browseButton.Text = MultiLang.Text(MultiLang.ID.ImportUI_Browse);
            localPathTextBox.Text = (path=="" ? Globals.ThisAddIn.Application.DefaultFilePath : path);
            okButton.Text = MultiLang.OKText();
            cancelButton.Text = MultiLang.CancelText();
            ToolTip t1 = new ToolTip();
            ToolTip t2 = new ToolTip();
            t1.SetToolTip(initialFilesButton,MultiLang.Text(MultiLang.ID.ImportUI_InitialFilesTooltip));
            t2.SetToolTip(currentFilesButton,MultiLang.Text(MultiLang.ID.ImportUI_CurrentFilesTooltip));
        }
        
        /// <summary>
        /// Tries to set vpl id and files choice, and closes the form with OK result.
        /// If vpl id is invalid, displays a warning and does not close the form.
        /// </summary>
        private void OKButton_Click(object sender,EventArgs e)
        {
            if(initialFilesButton.Checked)
                filesChoice = FilesChoice.Initial;
            else
                filesChoice = FilesChoice.Current;
            
            try{
                chosenVplID = Convert.ToInt32(vplIDTextBox.Text);
                if(chosenVplID<-1)
                    throw new Exception();
            }
            catch(Exception){
                MessageBox.Show(MultiLang.InvalidVplIDText(),MultiLang.InvalidVplIDText()+".",
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            // Verify the specified path (it has to be a valid rooted folder path)
            // Create it if it doens't exist
            chosenPath = localPathTextBox.Text;
            try{
                Path.GetFullPath(chosenPath); // Raises exception if not valid
                // Check if path is rooted and doesn't point to an existing file which is not a folder
                if(!Path.IsPathRooted(chosenPath) ||
                  (File.Exists(chosenPath) && !File.GetAttributes(chosenPath).HasFlag(FileAttributes.Directory)))
                    throw new Exception();
                
                // Try to create directory if it doesn't exist
                Directory.CreateDirectory(chosenPath);
            }
            catch(Exception){
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Import_InvalidFolder_) + " \"" + chosenPath + "\" " + MultiLang.Text(MultiLang.ID._Import_InvalidFolder),MultiLang.Text(MultiLang.ID.Import_InvalidFolder_Title),
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }

            formDialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// Closes the form without saving and sets result to Cancel.
        /// </summary>
        private void CancelButton_Click(object sender,EventArgs e)
        {
            formDialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Opens folder choice dialog and displays result in text box.
        /// </summary>
        private void BrowseButton_Click(object sender,EventArgs e)
        {
            DialogResult result = folderDialog.ShowDialog();
            if(result == DialogResult.OK){
                string path = folderDialog.SelectedPath;
                localPathTextBox.Text = path;
            }
        }
        
        /// <summary>
        /// Returns the DialogResult of the form.
        /// Should only be called after form has been displayed and closed.
        /// </summary>
        internal DialogResult GetDialogResult(){
            return formDialogResult;
        }

        /// <summary>
        /// Returns the user's files choice.
        /// Should only be called if form has been displayed and closed and DialogResult was OK.
        /// </summary>
        internal FilesChoice GetFilesChoice(){
            return filesChoice;
        }
        
        /// <summary>
        /// Returns the id of the vpl from which to import.
        /// Should only be called if form has been displayed and closed and DialogResult was OK.
        /// </summary>
        internal int GetChosenVplID(){
            return chosenVplID;
        }
        
        /// <summary>
        /// Returns the local path to import files to.
        /// Should only be called if form has been displayed and closed and DialogResult was OK.
        /// </summary>
        internal string GetChosenPath(){
            return chosenPath;
        }
    }
}
