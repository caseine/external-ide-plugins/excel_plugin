﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;

namespace CaseInEExcelAddIn
{
    public partial class CaseInERibbon
    {
        // Webservice url
        private const string WebServiceUrl = "https://moodle.caseine.org/webservice/rest/server.php";

        /// <summary>
        /// Loads text from MultiLang.
        /// </summary>
        public void ApplyLang(){
            // Ribbon UI text
            this.group1.Label = MultiLang.Text(MultiLang.ID.RibbonUI_Title);

            importButton.Label = MultiLang.Text(MultiLang.ID.RibbonUI_ImportLabel);
            importButton.SuperTip = MultiLang.Text(MultiLang.ID.RibbonUI_ImportTooltip);
            exportButton.Label = MultiLang.Text(MultiLang.ID.RibbonUI_ExportLabel);
            exportButton.SuperTip = MultiLang.Text(MultiLang.ID.RibbonUI_ExportTooltip);
            evaluateButton.Label = MultiLang.Text(MultiLang.ID.RibbonUI_EvaluateLabel);
            if(ExportOnEvaluation)
                evaluateButton.SuperTip = exportButton.SuperTip + MultiLang.Text(MultiLang.ID.RibbonUI_EvaluateTooltip);
            else
                evaluateButton.SuperTip = MultiLang.Text(MultiLang.ID.RibbonUI_EvaluateOnlyTooltip);
            evaluateButton.SuperTip = exportButton.SuperTip + MultiLang.Text(MultiLang.ID.RibbonUI_EvaluateTooltip);
            configButton.Label = MultiLang.Text(MultiLang.ID.RibbonUI_ConfigLabel);
            configButton.SuperTip = MultiLang.Text(MultiLang.ID.RibbonUI_ConfigTooltip);
            subjectButton.Label = MultiLang.Text(MultiLang.ID.RibbonUI_SubjectLabel);
            subjectButton.SuperTip = MultiLang.Text(MultiLang.ID.RibbonUI_SubjectTooltip);
        }
        
        /// <summary>
        /// Calls the specified service and returns a JsonCaseineObject parsed from Json response.
        /// For the "vpl_save" service, appends encoded_files to the URL.
        /// In case of trouble, displays an error message and throws WebException.
        /// Warranty : if no exception is raised, then result is non-null.
        /// </summary>
        /// <exception cref="WebException"></exception>
        private JsonCaseineObject CallService(string serviceName, string encodedFiles = ""){

            // Complete url
            string fullWebServiceUrl = WebServiceUrl + "?wsfunction=" + serviceName + "&id=" + VplID +
            "&wstoken=" + Token + "&moodlewsrestformat=json";
            
            JsonCaseineObject resultObject = null;
            bool retryConnection;
            do{
                retryConnection = false;
                try{
                    // Create HTTP request
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fullWebServiceUrl);
                    
                    // "vpl_save" function : send files via POST request
                    if(serviceName == "mod_vpl_save" && encodedFiles.Length>0){
                        request.Method = "POST";
                        byte[] filesAsBytes = Encoding.UTF8.GetBytes(encodedFiles);
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.ContentLength = filesAsBytes.Length;
                        using(Stream dataStream = request.GetRequestStream()){
                            dataStream.Write(filesAsBytes, 0, filesAsBytes.Length);
                            dataStream.Flush();
                        }
                    }

                    // Get response and read it
                    using(HttpWebResponse response = (HttpWebResponse)request.GetResponse()){
                    using(StreamReader reader = new StreamReader(response.GetResponseStream())){
                        string JSonResult = reader.ReadToEnd();
                        // Deserialize JSon reponse into CaseineObject
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        resultObject = (JsonCaseineObject)js.Deserialize(JSonResult,typeof(JsonCaseineObject));
                    }
                    }
                    
                }
                catch(Exception ex){
                    // Display connection error and ask to retry/cancel
                    string errorMessage = MultiLang.ConnectionErrorText() + ": \n" + ex.Message;
                    DialogResult retryPromptResult = MessageBox.Show(errorMessage,MultiLang.ConnectionErrorText(),
                        MessageBoxButtons.RetryCancel,MessageBoxIcon.Error);
                    retryConnection = (retryPromptResult == DialogResult.Retry);
                }
            }while(retryConnection);


            // Check for problems in resulting object

            if(resultObject == null && serviceName != "mod_vpl_save"){
                // Result shouldn't be null except for "vpl_save" service
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_GetDataError),MultiLang.ServiceErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                throw new WebException();
            }

            if(resultObject != null && resultObject.Exception != null){
                // Result shouldn't contain exception field
                MessageBox.Show(MultiLang.ServiceErrorText() + " (" + serviceName + ") :\n" + resultObject.Message,MultiLang.ServiceErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                throw new WebException();
            }
            
            return resultObject;
        }
        
        /// <summary>
        /// Retrieves requested files data from CaseInE through "vpl_info" service.
        /// Returns true iff files data has been correctly retrieved.
        /// </summary>
        private bool GetReqfiles(out CaseineFile[] reqfiles){
            JsonCaseineObject co;
            try{
                co = CallService("mod_vpl_info");
                reqfiles = co.Reqfiles;
            }
            catch(WebException){
                reqfiles = null;
                return false;
            }

            if(reqfiles==null){
                // This shouldn't happen
                MessageBox.Show("JSon parsing service error: reqfiles from vpl_info\n" +
                    "Please contact the support","Service error",
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;
            }
            if(reqfiles.Length==0){
                // If no requested files are specified, display a warning that some services may not work 
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_NoReqfilesWarning),MultiLang.Text(MultiLang.ID.Ribbon_NoReqfilesWarning_Title),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false; // as no file has been found
            }
            if(reqfiles.Length>=2){
                // If 2 or more requested files are specified, display a warning that some services may not work
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_TooManyReqfilesWarning),MultiLang.Text(MultiLang.ID.Ribbon_TooManyReqfilesWarning_Title),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return true; // as at least one file has been found
            }
            return true;
        }

        /// <summary>
        /// Checks existing files in path folder and eventually asks user for replace confirmation.
        /// Returns true iff confirmation has been granted or files don't exist yet.
        /// </summary>
        private static bool AskImportConfirmation(CaseineFile[] files, string path){
            // Check if files are already existing in directory
            string existingFiles = "";
            foreach(CaseineFile f in files){
                try{
                    string xlsfile = f.name;
                    if(Path.GetExtension(xlsfile) == ".b64"){
                        // Base64-encoded files are stored decoded on disk,
                        // so we check only for decoded files
                        xlsfile = Path.GetFileNameWithoutExtension(xlsfile);
                    }
                    if(File.Exists(Path.Combine(path,xlsfile)))
                        existingFiles = existingFiles + " " + xlsfile + "\n";
                }
                catch(Exception){
                    // Unknown problem on file name processing
                    // Do not do anything here, same problem will happen later in ImportFiles
                    // and will be treated there
                    continue;
                }
            }

            bool processImport = true;
            // If files are already existing at import location, ask for confirmation
            if(existingFiles.Length > 0){
                string importMessage = MultiLang.Text(MultiLang.ID.Ribbon_ConfirmImport) + " \"" + path + "\" :\n" + existingFiles;
                DialogResult result = MessageBox.Show(importMessage,MultiLang.Text(MultiLang.ID.RibbonUI_ImportLabel),MessageBoxButtons.YesNoCancel,MessageBoxIcon.Warning);
                processImport = (result == DialogResult.Yes);
            }
            return processImport;
        }
        
        /// <summary>
        /// Writes specified files into path folder.
        /// Returns true iff files were successfully written.
        /// </summary>
        private static bool WriteFiles(CaseineFile[] files, string path){
            bool do_import = AskImportConfirmation(files, path);

            if(do_import){
                bool import_ok = true;
                int nb_imported_files = 0;
                // Import (write) each requested file
                foreach(CaseineFile f in files){
                    if(f==null)
                        continue;
                    string xlsfile = f.name;
                    try{
                        if(Path.GetExtension(xlsfile)==".b64"){
                            // Base64-encoded files are stored decoded on disk
                            xlsfile = Path.GetFileNameWithoutExtension(xlsfile);
                        }
                    }
                    catch(Exception){
                        // Unknown problem on file name processing
                        // Display error message and ignore the file
                        MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_BadFilenameError) + " " + f.name,MultiLang.Text(MultiLang.ID.Ribbon_BadFilenameError),
                            MessageBoxButtons.OK,MessageBoxIcon.Error);
                        continue;
                    }
                    // Close any open workbook with the same name than one of imported files
                    foreach(Workbook workbook in Globals.ThisAddIn.Application.Workbooks){
                        if(workbook.Name==xlsfile){
                            workbook.Close(false);
                        }
                    }
                    bool retry_import;
                    bool abort_import = false;
                    do{
                        retry_import = false;
                        try{
                            // Create or replace file and write data
                            string fullname = Path.Combine(path,xlsfile);
                            // xls files are base64-encoded
                            File.WriteAllBytes(fullname, Convert.FromBase64String(f.data));
                        }
                        catch(Exception ex){
                            // Display error and ask to abort/retry/ignore
                            string error_message = MultiLang.Text(MultiLang.ID.Ribbon_ImportError) + " \"" +
                            xlsfile + "\" : \n" + ex.Message;
                            var import_error_result = MessageBox.Show(error_message,MultiLang.ErrorText(),
                                MessageBoxButtons.AbortRetryIgnore,MessageBoxIcon.Error);
                            if(import_error_result == DialogResult.Retry){
                                // Retry this file
                                retry_import = true;
                            }
                            else if(import_error_result == DialogResult.Abort){
                                // Abort import of remaining files
                                abort_import = true;
                                import_ok = false;
                            }
                        }
                    }while(retry_import);
                    if(abort_import)
                        break;
                    
                    nb_imported_files++;
                }
                if(import_ok && nb_imported_files>0)
                    MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_ImportSuccess),MultiLang.Text(MultiLang.ID.Ribbon_ImportSuccess_Title),
                        MessageBoxButtons.OK,MessageBoxIcon.Information);
                return import_ok;
            }
            return false;
        }
        
        /// <summary>
        /// Imports (pull) initial VPL files.
        /// Throws IOException if unable to open imported file.
        /// Returns true iff files were successfully imported and written.
        /// </summary>
        /// <exception cref="IOException"></exception>
        private bool ImportInitialFiles()
        {
            // Retrieve initial (requested) files data
            if(!GetReqfiles(out CaseineFile[] reqfiles))
                return false;
            
            // Import (write files)
            bool writeOk = WriteFiles(reqfiles,ImportLocation);
            
            // Open the imported workbook
            if(writeOk){
                string filename = reqfiles[0].name;
                if(Path.GetExtension(filename) == ".b64"){
                    filename = Path.GetFileNameWithoutExtension(filename);
                }
				Globals.ThisAddIn.Application.Workbooks.Open(Path.Combine(ImportLocation,filename));
            }

            return writeOk;
        }

		/// <summary>
		/// Imports (pull) student current VPL files on CaseInE.
		/// Throws IOException if unable to open imported file.
		/// Returns true iff files were successfully imported and written.
		/// </summary>
		/// <exception cref="IOException"></exception>
		private bool ImportCurrentFiles()
        {
            // Retrieve current files data
            JsonCaseineObject co = null;
            try{
                co = CallService("mod_vpl_open");
            }
            catch(WebException){
                return false;
            }

            if(co.Files==null){
                // This shouldn't happen
                MessageBox.Show("JSon parsing service error: files from vpl_open\n" +
                    "Please contact the support","Service error",
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;
            }
            if(co.Files.Length==0){
                // Happens only when there are no requested files and no submission yet (files = reqfiles)
                // Display a warning that some services may not work
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_NoReqfilesWarning),MultiLang.Text(MultiLang.ID.Ribbon_NoReqfilesWarning_Title),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return false;
            }
            
            // Import (write files)
            bool writeOk = WriteFiles(co.Files,ImportLocation);
            
            // Open workbook
            // If several files are imported, only the first one is open
            if(writeOk){
                string filename = co.Files[0].name;
                if(Path.GetExtension(filename) == ".b64"){
                    filename = Path.GetFileNameWithoutExtension(filename);
                }
				Globals.ThisAddIn.Application.Workbooks.Open(Path.Combine(ImportLocation,filename));
			}

            return writeOk;
        }
        
        /// <summary>
        /// Exports (push) the current workbook.
        /// Prompts to save it if it is not saved on disk.
        /// Returns true iff file was successfully exported.
        /// </summary>
        private bool ExportFile(bool displayInfo=true){
            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            if(workbook.Path.Length==0){
                // Path is empty, workbook isn't saved on disk
                // Prompt to save it
                DialogResult result = MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_PromptSaveAs),MultiLang.Text(MultiLang.ID.Ribbon_PromptSaveAs_Title),
                        MessageBoxButtons.YesNoCancel,MessageBoxIcon.Warning);
                if(result!=DialogResult.Yes)
                    return false;
                dynamic saveAsName = workbook.Application.GetSaveAsFilename();
                try{
                    if(saveAsName == false)
                        return false;
                }
                catch(Exception){
                    // Exception has been caught : saveAsName is not of type bool, it should be a file name
                    try{
                        workbook.SaveAs(saveAsName);
                    }
                    catch(Exception){
                        // File hasn't been saved, maybe because of declined conflicts
                        // In any case, cancel
                        return false;
                    }
                }
            }
            else{
                // Path is non empty, workbook is saved on disk
                if(AutosaveOnExport){
                    workbook.Save();
                }
                else if(!workbook.Saved){
                    DialogResult result = MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_PromptSave),MultiLang.Text(MultiLang.ID.Ribbon_PromptSave_Title),
                        MessageBoxButtons.YesNoCancel,MessageBoxIcon.Warning);
                    if(result==DialogResult.Cancel)
                        return false;
                    else if(result==DialogResult.Yes){
                        workbook.Save();
                    }
                }
            }

            // Retrieve the name of requested file
            if(!GetReqfiles(out CaseineFile[] reqfiles))
                return false;
            
            string reqfileName = reqfiles[0].name;
            
            // Encode the file and send it under the name of the requested file
            string fullname = workbook.FullName;
            string content;
            // Set up the rights so that we can read the file while it is open in Excel
            using(FileStream fs = new FileStream(fullname, FileMode.Open,FileAccess.Read, FileShare.ReadWrite)){
                byte[] binContent = new byte[fs.Length];
                fs.Read(binContent,0,(int)fs.Length);
                content = Convert.ToBase64String(binContent);
            }
            // Encode name and data into URL-formated strings
            string fileNameUrl = HttpUtility.UrlEncode(reqfileName,new UTF8Encoding());
            string fileDataUrl = HttpUtility.UrlEncode(content,new UTF8Encoding());
            bool decodeBase64 = Path.GetExtension(reqfileName) != ".b64"; // Legacy: if the required file is base64-encoded on the server, do not decode it.
            string encodedFile = "files[0][name]=" + fileNameUrl
                                + "&files[0][data]=" + fileDataUrl
                                + "&files[0][isbinary]=" + (decodeBase64 ? "1" : "0");
            
            // Call the service
            try{
                CallService("mod_vpl_save",encodedFile);
            }
            catch(WebException){
                return false;
            }

			if(displayInfo){
				MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_ExportSuccess),MultiLang.Text(MultiLang.ID.Ribbon_ExportSuccess_Title),
							  MessageBoxButtons.OK,MessageBoxIcon.Information);
			}

            return true;
        }

        /// <summary>
        /// Parses the cell name and sheet name from the "Input" line from evaluation result.
        /// Returns true iff line has been parsed correctly.
        /// </summary>
        private bool ParseEvaluationInputLine(string line, out string cellName, out string sheetName){
            cellName = "";
            sheetName = "";
            // Line template should be : "Input : <cell> in sheet <sheet_name>"
            if(line.StartsWith("Input")){
                int i_string = 5;
                while(i_string<line.Length && (line[i_string] == ' ' || line[i_string] == ':')){
                    i_string++;
                }
                line = line.Substring(i_string);
                if(line.StartsWith("Cell")){
                    line = line.Substring(4);
                    line = line.Trim();
                    i_string = 0;
                    int cellNameLength = 0;
                    while(i_string + cellNameLength<line.Length && line[i_string + cellNameLength] != ' ')
                        cellNameLength++;
                    cellName = line.Substring(i_string,cellNameLength);
                    line = line.Substring(i_string+cellNameLength);

                    if(line.StartsWith(" in sheet")){
                        line = line.Substring(9);
                        sheetName = line.Trim();
                        return true;
                    }
                }
            }
            return false;
        }
		
		/// <summary>
		/// Writes the evaluation result into new worksheets,
		/// named "Evaluation result" and "&lt;Sheet&gt;_EvalOverview".
		/// </summary>
		private void WriteEvaluationResult(JsonCaseineObject co){
            if(co.Compilation == null || co.Evaluation == null || co.Grade == null){
                // This shouldn't happen
                MessageBox.Show("JSon parsing service error: result from vpl_get_result\n" +
                    "Please contact the support","Service error",
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            
            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            Sheets worksheets = workbook.Worksheets;
            bool wasSaved = workbook.Saved;
            
            // Remove old evaluation sheets
            List<Worksheet> toBeDeleted = new List<Worksheet>();
            foreach(Worksheet sheet in worksheets){
                foreach(CustomProperty p in sheet.CustomProperties){
                    if(p.Name == "IsCaseInEEval" && p.Value=="true"){
                        toBeDeleted.Add(sheet);
                    }
                }
            }
            try{
                bool displayAlerts = Globals.ThisAddIn.Application.DisplayAlerts;
                Globals.ThisAddIn.Application.DisplayAlerts = false;
                foreach(Worksheet oldEvalSheet in toBeDeleted){
                    oldEvalSheet.Delete();
                }
                Globals.ThisAddIn.Application.DisplayAlerts = displayAlerts;
            }
            catch(Exception){
                MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_CellInEditionError),MultiLang.ErrorText(),
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            // Append the new evaluation sheet at the end of the workbook
            Worksheet rightmostSheet = worksheets[worksheets.Count];
            // The first sheet (evaluation result) is a new blank sheet
            Worksheet evalSheet = worksheets.Add(After:rightmostSheet);
            evalSheet.Name = "EvaluationResult";
            evalSheet.CustomProperties.Add("IsCaseInEEval","true");

            int line = 1;
            // If there are compilation messages, display them
            if(co.Compilation.Length > 0){
                evalSheet.Cells[line,1] = "Compilation :";
                evalSheet.Cells[line,2] = co.Compilation;
                line++;
            }
            // Display grade with grade-dependant background color
            evalSheet.Cells[line,1] = "Grade :";
            try{
                evalSheet.Cells[line,2] = co.Grade.Substring(16);
                int grade = 0;
                int i=16;
                while(co.Grade[i]>='0' && co.Grade[i]<='9'){
                    grade = grade*10 + co.Grade[i]-'0';
                    i++;
                }
                XlRgbColor cellColor;
                if(grade<30)
                    cellColor = XlRgbColor.rgbOrangeRed;
                else if(grade<80)
                    cellColor = XlRgbColor.rgbOrange;
                else
                    cellColor = XlRgbColor.rgbLightGreen;
                evalSheet.Cells[line,2].Interior.Color = cellColor;
            }
            catch(Exception){
                evalSheet.Cells[line,2] = co.Grade;
            }

            // Write results for each test
            // In case of invalidated test, display the evaluated cell in overview
            evalSheet.Cells[1,2].EntireColumn.ColumnWidth = 50;
			evalSheet.Cells.Borders.LineStyle = XlLineStyle.xlContinuous;
			evalSheet.Cells.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
			evalSheet.Cells.Borders.Color = XlRgbColor.rgbGray;

			List <Worksheet> overviewSheets = new List<Worksheet>();

            using(StringReader reader = new StringReader(co.Evaluation)){
                string evalLine = reader.ReadLine();
				Range cell = null; // Cell designation of the current invalidated test
				Worksheet overviewSheet = null; // Overview sheet associated with the current invalidated test
                evalSheet.Cells[line+1,1] = "Evaluation :";
                while(evalLine != null){
					if (evalLine.StartsWith("-Tests results")){
						// This is the header line
						line++;
						evalSheet.Cells[line, 2] = evalLine;
						evalSheet.Cells[line, 2].Interior.Color = XlRgbColor.rgbLightGray;
					}
					else if (evalLine.StartsWith("-")){
						// This is the first line of a test : move to next cell to write it
                        line++;
                        evalSheet.Cells[line,2] = evalLine;
                        evalSheet.Cells[line,2].Interior.Color = XlRgbColor.rgbLightGreen;
						cell = null;
						overviewSheet = null;
					}
                    else{
						// This test has more than one line : it is a invalidated test
                        string currentCellContent = Convert.ToString(evalSheet.Cells[line,2].Value2);
                        evalSheet.Cells[line,2] = currentCellContent + "\n" + evalLine;
                        evalSheet.Cells[line,2].Interior.Color = XlRgbColor.rgbLightCoral;
                        if(evalLine.StartsWith("Input")){
                            // Display the cell where the mistake is on overview
                            if(ParseEvaluationInputLine(evalLine,out string cellName,out string sheetName)){
                                string overviewSheetName = sheetName + "_EvalOverview";
                                foreach(Worksheet worksheet in overviewSheets){
                                    if(worksheet.Name == overviewSheetName){
                                        overviewSheet = worksheet;
                                    }
                                }
                                if(overviewSheet == null){
                                    // Overview sheet doesn't exist yet, create it
                                    Worksheet baseSheet = worksheets[sheetName];
                                    baseSheet.Copy(After:baseSheet);
                                    overviewSheet = worksheets[baseSheet.Index+1];
                                    overviewSheet.Name = overviewSheetName;
                                    overviewSheet.CustomProperties.Add("IsCaseInEEval","true");
                                    overviewSheets.Add(overviewSheet);
                                }
                                    
                                cell = overviewSheet.Evaluate(cellName);
                                if(cell != null){
									// Set the font in red bold
									cell.Font.Bold = true;
									cell.Font.Color = XlRgbColor.rgbRed;
									// Add a comment "wrong answer"
									cell.AddComment(MultiLang.Text(MultiLang.ID.Eval_WrongAnswer));
                                }
                            }
                        }
						else if(evalLine.StartsWith("Expected Output:")){
							// Set the comment to show the expected answer - could be "hidden"
							string expectedAnswer = evalLine.Substring(16).Trim();
							if(cell != null){
								// Set the comment text (cell.AddComment() doesnt work if a comment is already there)
								cell.Comment.Text(cell.Comment.Text() + "\n" + MultiLang.Text(MultiLang.ID.Eval_ExpectedAnswer) + " " + expectedAnswer);
							}
						}
                    }
                    evalLine = reader.ReadLine();
                }
            }
			
			// Set overview and result sheets in read-only for user
			evalSheet.Protect();
			foreach(Worksheet overview in overviewSheets){
				overview.Protect();
			}

            if(overviewSheets.Count>0){
				// Activate the first overview sheet
				overviewSheets[0].Activate();
			}

            if(wasSaved)
                workbook.Save();
			

			string gradeText;
            try{
                gradeText = co.Grade.Substring(16);
            }
            catch(ArgumentOutOfRangeException){
                gradeText = co.Grade;
            }

			MessageBox.Show(MultiLang.Text(MultiLang.ID.Eval_Grade) + " " + gradeText,MultiLang.Text(MultiLang.ID.Eval_GradeTitle),
				MessageBoxButtons.OK,MessageBoxIcon.Information);
        }

		/// <summary>
		/// Calls evaluate service, retrieves the result and write it in workbook.
		/// </summary>
		private void Evaluate(){
            JsonCaseineObject co;
            bool retryEvaluate;
            // Delay between evaluate and get_result
            int millisecondsDelay = 4000; // 1000 is for demo, put it at 4000 or 5000
            do{
                retryEvaluate = false;
                try{
                    CallService("mod_vpl_evaluate");
                }
                catch(WebException){
                    return;
                }
                
                // Set up an event for the "cancel" button on the wait form
                using(AutoResetEvent waitCancelled = new AutoResetEvent(false)){
                    Thread waitUIThread = new Thread(new ThreadStart(delegate{
                        using(FormWait formWait = new FormWait()){
                        using(System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer()){
                            timer.Tick += delegate{
                                // close the form after wait time
                                // this method puts DialogResult to Cancel,
                                // so actual cancel button is set up to Abort.
                                formWait.Close();
                            };
                            timer.Interval = millisecondsDelay;
                            // Start the timer and display the wait form
                            timer.Start();
                            var result = formWait.ShowDialog();
                            if(result == DialogResult.Abort){
                                // The user cancelled the evaluation - set the event
                                waitCancelled.Set();
                            }
                        }
                        }
                    }));

                    waitUIThread.Start();

                    try{
                        bool isCancelled = waitCancelled.WaitOne(millisecondsDelay);
                        if(isCancelled)
                            return;
                        co = CallService("mod_vpl_get_result");
                    }
                    catch(WebException){
                        waitUIThread.Abort();
                        return;
                    }
                }

                if(co.Grade.Length == 0){
                    // Evaluation result is empty
                    // Prompt to retry with greater delay
                    var result = MessageBox.Show(MultiLang.Text(MultiLang.ID.Ribbon_PromptEvaluationRetry),MultiLang.ServiceErrorText(),
                        MessageBoxButtons.RetryCancel,MessageBoxIcon.Error);
                    if(result == DialogResult.Retry){
                        retryEvaluate = true;
                        millisecondsDelay += 3000;
                    }
                    else
                        return;
                }
            }while(retryEvaluate);
            
            // Write the result in workbook
            WriteEvaluationResult(co);
        }

        /// <summary>
        /// Opens a FormConfig and returns the dialog result.
        /// </summary>
        private DialogResult OpenConfigForm(){
            DialogResult result;
            using(FormConfig form = new FormConfig(this)){
                form.ShowDialog();
                result = form.GetDialogResult();
            }
            return result;
        }

        /// <summary>
        /// Displays the exercise subject description in a separate window
        /// </summary>
        private void DisplaySubject(){
            JsonCaseineObject co = null;
            try{
                co = CallService("mod_vpl_info");
            }
            catch(WebException){
                return;
            }

            if(co.Name == null || co.Intro == null || co.Shortdescription == null){
                // This shouldn't happen
                MessageBox.Show("JSon parsing service error: name or intro or shortdescription from vpl_info\n" +
                    "Please contact the support","Service error",
                    MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            // Display a non-locking new window
            Thread t = new Thread(new ThreadStart(delegate
            {
                using(FormSubject form = new FormSubject(co.Name + " - " + co.Shortdescription,co.Intro)){
                    form.ShowDialog();
                }
            }));
            t.SetApartmentState(ApartmentState.STA); // necessary for embedded WebBrowser control
            t.Start();
        }

        /// <summary>
        /// Writes id to active workbook's metadata
        /// </summary>
        private void ApplyVplIDToWorkbook(int id){
			Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            if(workbook.Path.Length > 0){
                // Path is non empty, the workbook is saved on the disk
                bool was_saved = workbook.Saved;
                bool found = false;
                foreach(DocumentProperty p in workbook.CustomDocumentProperties){
                    if(p.Name==VplIDMetadataName){
                        p.Value = id;
                        found = true;
                    }
                }
                if(!found){
                    workbook.CustomDocumentProperties.Add(VplIDMetadataName,false,MsoDocProperties.msoPropertyTypeNumber,id);
                }
                if(was_saved)
                    workbook.Save();
            }
        }
    }
}