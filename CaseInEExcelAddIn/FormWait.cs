﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaseInEExcelAddIn
{
    public partial class FormWait:Form
    {
        public FormWait()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Form initialization : sets up the progress bar as cyclic progress bar
        /// </summary>
        private void FormWait_Load(object sender,EventArgs e)
        {
            cyclicProgressBar.Style = ProgressBarStyle.Marquee;
            cyclicProgressBar.MarqueeAnimationSpeed = 20;

            // Load text from MultiLang
            this.Text = MultiLang.Text(MultiLang.ID.WaitUI_Title);
            label1.Text = MultiLang.Text(MultiLang.ID.WaitUI_Message);
            cancelButton.Text = MultiLang.CancelText();
        }
    }
}
