﻿namespace CaseInEExcelAddIn
{
    partial class FormConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfig));
			this.welcomeLabel = new System.Windows.Forms.Label();
			this.tokenLabel = new System.Windows.Forms.Label();
			this.tokenTextbox = new System.Windows.Forms.TextBox();
			this.saveButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.rememberMeCheck = new System.Windows.Forms.CheckBox();
			this.advancedOptionsContainer = new System.Windows.Forms.SplitContainer();
			this.advancedLabel = new System.Windows.Forms.Label();
			this.advancedButton = new System.Windows.Forms.Button();
			this.promptVplIDOnExportCheck = new System.Windows.Forms.CheckBox();
			this.autoSaveOnExportCheck = new System.Windows.Forms.CheckBox();
			this.exportOnEvaluationCheck = new System.Windows.Forms.CheckBox();
			this.FlagBox = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.flagPanel = new System.Windows.Forms.Panel();
			this.enFlagBox = new System.Windows.Forms.PictureBox();
			this.frFlagBox = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.advancedOptionsContainer)).BeginInit();
			this.advancedOptionsContainer.Panel1.SuspendLayout();
			this.advancedOptionsContainer.Panel2.SuspendLayout();
			this.advancedOptionsContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.FlagBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.flagPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.enFlagBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frFlagBox)).BeginInit();
			this.SuspendLayout();
			// 
			// welcomeLabel
			// 
			this.welcomeLabel.AutoSize = true;
			this.welcomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.welcomeLabel.Location = new System.Drawing.Point(41, 16);
			this.welcomeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.welcomeLabel.Name = "welcomeLabel";
			this.welcomeLabel.Size = new System.Drawing.Size(71, 20);
			this.welcomeLabel.TabIndex = 1;
			this.welcomeLabel.Text = "welcome";
			// 
			// tokenLabel
			// 
			this.tokenLabel.AutoSize = true;
			this.tokenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tokenLabel.Location = new System.Drawing.Point(41, 59);
			this.tokenLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.tokenLabel.Name = "tokenLabel";
			this.tokenLabel.Size = new System.Drawing.Size(45, 18);
			this.tokenLabel.TabIndex = 4;
			this.tokenLabel.Text = "token";
			// 
			// tokenTextbox
			// 
			this.tokenTextbox.Location = new System.Drawing.Point(107, 59);
			this.tokenTextbox.Margin = new System.Windows.Forms.Padding(2);
			this.tokenTextbox.Name = "tokenTextbox";
			this.tokenTextbox.Size = new System.Drawing.Size(417, 20);
			this.tokenTextbox.TabIndex = 5;
			// 
			// saveButton
			// 
			this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.saveButton.Location = new System.Drawing.Point(318, 190);
			this.saveButton.Margin = new System.Windows.Forms.Padding(2);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(103, 29);
			this.saveButton.TabIndex = 11;
			this.saveButton.Text = "save";
			this.saveButton.UseVisualStyleBackColor = true;
			this.saveButton.Click += new System.EventHandler(this.SaveButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.CausesValidation = false;
			this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cancelButton.Location = new System.Drawing.Point(425, 190);
			this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(103, 29);
			this.cancelButton.TabIndex = 12;
			this.cancelButton.Text = "cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// rememberMeCheck
			// 
			this.rememberMeCheck.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.rememberMeCheck.AutoSize = true;
			this.rememberMeCheck.Checked = true;
			this.rememberMeCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.rememberMeCheck.Location = new System.Drawing.Point(448, 83);
			this.rememberMeCheck.Margin = new System.Windows.Forms.Padding(2);
			this.rememberMeCheck.Name = "rememberMeCheck";
			this.rememberMeCheck.Size = new System.Drawing.Size(76, 17);
			this.rememberMeCheck.TabIndex = 6;
			this.rememberMeCheck.Text = "savetoken";
			this.rememberMeCheck.UseVisualStyleBackColor = true;
			// 
			// advancedOptionsContainer
			// 
			this.advancedOptionsContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.advancedOptionsContainer.IsSplitterFixed = true;
			this.advancedOptionsContainer.Location = new System.Drawing.Point(8, 92);
			this.advancedOptionsContainer.Margin = new System.Windows.Forms.Padding(2);
			this.advancedOptionsContainer.Name = "advancedOptionsContainer";
			this.advancedOptionsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// advancedOptionsContainer.Panel1
			// 
			this.advancedOptionsContainer.Panel1.Controls.Add(this.advancedLabel);
			this.advancedOptionsContainer.Panel1.Controls.Add(this.advancedButton);
			// 
			// advancedOptionsContainer.Panel2
			// 
			this.advancedOptionsContainer.Panel2.Controls.Add(this.promptVplIDOnExportCheck);
			this.advancedOptionsContainer.Panel2.Controls.Add(this.autoSaveOnExportCheck);
			this.advancedOptionsContainer.Panel2.Controls.Add(this.exportOnEvaluationCheck);
			this.advancedOptionsContainer.Size = new System.Drawing.Size(283, 89);
			this.advancedOptionsContainer.SplitterDistance = 25;
			this.advancedOptionsContainer.SplitterWidth = 3;
			this.advancedOptionsContainer.TabIndex = 10;
			// 
			// advancedLabel
			// 
			this.advancedLabel.AutoSize = true;
			this.advancedLabel.Location = new System.Drawing.Point(24, 4);
			this.advancedLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.advancedLabel.Name = "advancedLabel";
			this.advancedLabel.Size = new System.Drawing.Size(55, 13);
			this.advancedLabel.TabIndex = 1;
			this.advancedLabel.Text = "advanced";
			// 
			// advancedButton
			// 
			this.advancedButton.BackgroundImage = global::CaseInEExcelAddIn.Properties.Resources.caseine_collapsed_arrow;
			this.advancedButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.advancedButton.Location = new System.Drawing.Point(3, 3);
			this.advancedButton.Margin = new System.Windows.Forms.Padding(2);
			this.advancedButton.Name = "advancedButton";
			this.advancedButton.Size = new System.Drawing.Size(16, 16);
			this.advancedButton.TabIndex = 0;
			this.advancedButton.UseVisualStyleBackColor = true;
			this.advancedButton.Click += new System.EventHandler(this.AdvancedButton_Click);
			// 
			// promptVplIDOnExportCheck
			// 
			this.promptVplIDOnExportCheck.AutoSize = true;
			this.promptVplIDOnExportCheck.Checked = true;
			this.promptVplIDOnExportCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.promptVplIDOnExportCheck.Location = new System.Drawing.Point(3, 42);
			this.promptVplIDOnExportCheck.Margin = new System.Windows.Forms.Padding(2);
			this.promptVplIDOnExportCheck.Name = "promptVplIDOnExportCheck";
			this.promptVplIDOnExportCheck.Size = new System.Drawing.Size(168, 17);
			this.promptVplIDOnExportCheck.TabIndex = 2;
			this.promptVplIDOnExportCheck.Text = "always prompt vpl id on export";
			this.promptVplIDOnExportCheck.UseVisualStyleBackColor = true;
			// 
			// autoSaveOnExportCheck
			// 
			this.autoSaveOnExportCheck.AutoSize = true;
			this.autoSaveOnExportCheck.Location = new System.Drawing.Point(3, 21);
			this.autoSaveOnExportCheck.Margin = new System.Windows.Forms.Padding(2);
			this.autoSaveOnExportCheck.Name = "autoSaveOnExportCheck";
			this.autoSaveOnExportCheck.Size = new System.Drawing.Size(120, 17);
			this.autoSaveOnExportCheck.TabIndex = 1;
			this.autoSaveOnExportCheck.Text = "auto save on export";
			this.autoSaveOnExportCheck.UseVisualStyleBackColor = true;
			// 
			// exportOnEvaluationCheck
			// 
			this.exportOnEvaluationCheck.AutoSize = true;
			this.exportOnEvaluationCheck.Checked = true;
			this.exportOnEvaluationCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.exportOnEvaluationCheck.Location = new System.Drawing.Point(3, 2);
			this.exportOnEvaluationCheck.Margin = new System.Windows.Forms.Padding(2);
			this.exportOnEvaluationCheck.Name = "exportOnEvaluationCheck";
			this.exportOnEvaluationCheck.Size = new System.Drawing.Size(93, 17);
			this.exportOnEvaluationCheck.TabIndex = 0;
			this.exportOnEvaluationCheck.Text = "export on eval";
			this.exportOnEvaluationCheck.UseVisualStyleBackColor = true;
			// 
			// FlagBox
			// 
			this.FlagBox.Location = new System.Drawing.Point(8, 190);
			this.FlagBox.Margin = new System.Windows.Forms.Padding(2);
			this.FlagBox.Name = "FlagBox";
			this.FlagBox.Size = new System.Drawing.Size(34, 27);
			this.FlagBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.FlagBox.TabIndex = 13;
			this.FlagBox.TabStop = false;
			this.FlagBox.Click += new System.EventHandler(this.FlagBox_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine;
			this.pictureBox1.Location = new System.Drawing.Point(8, 8);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(29, 48);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// flagPanel
			// 
			this.flagPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.flagPanel.Controls.Add(this.enFlagBox);
			this.flagPanel.Controls.Add(this.frFlagBox);
			this.flagPanel.Enabled = false;
			this.flagPanel.Location = new System.Drawing.Point(8, 185);
			this.flagPanel.Margin = new System.Windows.Forms.Padding(2);
			this.flagPanel.Name = "flagPanel";
			this.flagPanel.Size = new System.Drawing.Size(79, 34);
			this.flagPanel.TabIndex = 14;
			this.flagPanel.Visible = false;
			// 
			// enFlagBox
			// 
			this.enFlagBox.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine_en_flag;
			this.enFlagBox.Location = new System.Drawing.Point(40, 2);
			this.enFlagBox.Margin = new System.Windows.Forms.Padding(2);
			this.enFlagBox.Name = "enFlagBox";
			this.enFlagBox.Size = new System.Drawing.Size(34, 27);
			this.enFlagBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.enFlagBox.TabIndex = 16;
			this.enFlagBox.TabStop = false;
			this.enFlagBox.Click += new System.EventHandler(this.EnFlagBox_Click);
			// 
			// frFlagBox
			// 
			this.frFlagBox.Image = global::CaseInEExcelAddIn.Properties.Resources.caseine_fr_flag;
			this.frFlagBox.Location = new System.Drawing.Point(2, 2);
			this.frFlagBox.Margin = new System.Windows.Forms.Padding(2);
			this.frFlagBox.Name = "frFlagBox";
			this.frFlagBox.Size = new System.Drawing.Size(34, 27);
			this.frFlagBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.frFlagBox.TabIndex = 15;
			this.frFlagBox.TabStop = false;
			this.frFlagBox.Click += new System.EventHandler(this.FrFlagBox_Click);
			// 
			// FormConfig
			// 
			this.AcceptButton = this.saveButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(535, 226);
			this.Controls.Add(this.flagPanel);
			this.Controls.Add(this.FlagBox);
			this.Controls.Add(this.advancedOptionsContainer);
			this.Controls.Add(this.rememberMeCheck);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.tokenTextbox);
			this.Controls.Add(this.tokenLabel);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.welcomeLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormConfig";
			this.Text = "config";
			this.Load += new System.EventHandler(this.FormConfig_Load);
			this.advancedOptionsContainer.Panel1.ResumeLayout(false);
			this.advancedOptionsContainer.Panel1.PerformLayout();
			this.advancedOptionsContainer.Panel2.ResumeLayout(false);
			this.advancedOptionsContainer.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.advancedOptionsContainer)).EndInit();
			this.advancedOptionsContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.FlagBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.flagPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.enFlagBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frFlagBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label welcomeLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label tokenLabel;
        private System.Windows.Forms.TextBox tokenTextbox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox rememberMeCheck;
        private System.Windows.Forms.SplitContainer advancedOptionsContainer;
        private System.Windows.Forms.Label advancedLabel;
        private System.Windows.Forms.Button advancedButton;
        private System.Windows.Forms.CheckBox exportOnEvaluationCheck;
        private System.Windows.Forms.PictureBox FlagBox;
        private System.Windows.Forms.Panel flagPanel;
        private System.Windows.Forms.PictureBox enFlagBox;
        private System.Windows.Forms.PictureBox frFlagBox;
        private System.Windows.Forms.CheckBox autoSaveOnExportCheck;
        private System.Windows.Forms.CheckBox promptVplIDOnExportCheck;
    }
}