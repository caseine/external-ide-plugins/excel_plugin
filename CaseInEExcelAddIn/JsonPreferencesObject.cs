﻿namespace CaseInEExcelAddIn
{
    internal class JsonPreferencesObject
    {
        public bool RememberToken {get;set;}
        public string Lang {get;set;}
        public bool ExportOnEvaluation {get;set;}
        public bool AutosaveOnExport {get;set;}
        public bool PromptVplIDOnExport {get;set;}
        public string ImportLocation {get;set;}

        public JsonPreferencesObject(){
        }

        public JsonPreferencesObject(bool RememberToken, string Lang, bool ExportOnEvaluation, bool AutosaveOnExport, bool PromptVplIDOnExport, string ImportLocation){
            this.RememberToken = RememberToken;
            this.Lang = Lang;
            this.ExportOnEvaluation = ExportOnEvaluation;
            this.AutosaveOnExport = AutosaveOnExport;
            this.PromptVplIDOnExport = PromptVplIDOnExport;
            this.ImportLocation = ImportLocation;
        }
    }
}
