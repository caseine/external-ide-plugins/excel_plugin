﻿using System;
using System.Windows.Forms;

namespace CaseInEExcelAddIn
{
    public partial class FormExport:Form
    {
        // Result of the dialog - done manually to prevent unverified validation
        private DialogResult formDialogResult;
        // Choice for vpl id
        private int chosenVplID;

        /// <summary>
        /// Form initialization.
        /// </summary>
        public FormExport(int vplID=-1)
        {
            InitializeComponent();
            
            formDialogResult = DialogResult.None;
            
            // Load text from MultiLang
            this.Text = MultiLang.Text(MultiLang.ID.ExportUI_Title);
            label1.Text = MultiLang.Text(MultiLang.ID.ExportUI_VplIDPrompt);
            vplIDTextBox.Text = (vplID==-1 ? "" : vplID.ToString());
            okButton.Text = MultiLang.OKText();
            cancelButton.Text = MultiLang.CancelText();
        }
        
        /// <summary>
        /// Tries to set vpl id, and closes the form with OK result.
        /// If vpl id is invalid, displays a warning and does not close the form.
        /// </summary>
        private void OKButton_Click(object sender,EventArgs e)
        {
            try{
                chosenVplID = Convert.ToInt32(vplIDTextBox.Text);
                if(chosenVplID<-1)
                    throw new Exception();
            }
            catch(Exception){
                MessageBox.Show(MultiLang.InvalidVplIDText(),MultiLang.InvalidVplIDText()+".",
                    MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }
            formDialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// Closes the form without saving and sets result to Cancel.
        /// </summary>
        private void CancelButton_Click(object sender,EventArgs e)
        {
            formDialogResult = DialogResult.Cancel;
            this.Close();
        }
        
        /// <summary>
        /// Returns the DialogResult of the form.
        /// Should only be called after form has been displayed and closed.
        /// </summary>
        internal DialogResult GetDialogResult(){
            return formDialogResult;
        }
        
        /// <summary>
        /// Returns the id of the vpl from which to import.
        /// Should only be called if form has been displayed and closed and DialogResult was OK.
        /// </summary>
        internal int GetChosenVplID(){
            return chosenVplID;
        }
    }
}
